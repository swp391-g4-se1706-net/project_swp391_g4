/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author myth
 */
public class News {
    int id;
    String title;
    String CreateDate;
    String ModifiDate;
    String CreateBy;
    String ModifyBy;
    String Content;
    int CategoryID;

    public News() {
        connect();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public News(int id, String title, String CreateDate, String ModifiDate, String CreateBy, String ModifyBy, String Content, int CategoryID) {
        this.id = id;
        this.title = title;
        this.CreateDate = CreateDate;
        this.ModifiDate = ModifiDate;
        this.CreateBy = CreateBy;
        this.ModifyBy = ModifyBy;
        this.Content = Content;
        this.CategoryID = CategoryID;
        connect();
    }
    public News( String title, String CreateDate, String ModifiDate, String CreateBy, String ModifyBy, String Content, int CategoryID) {
        this.title = title;
        this.CreateDate = CreateDate;
        this.ModifiDate = ModifiDate;
        this.CreateBy = CreateBy;
        this.ModifyBy = ModifyBy;
        this.Content = Content;
        this.CategoryID = CategoryID;
        connect();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public String getModifiDate() {
        return ModifiDate;
    }

    public void setModifiDate(String ModifiDate) {
        this.ModifiDate = ModifiDate;
    }

    public String getCreateBy() {
        return CreateBy;
    }

    public void setCreateBy(String CreateBy) {
        this.CreateBy = CreateBy;
    }

    public String getModifyBy() {
        return ModifyBy;
    }

    public void setModifyBy(String ModifyBy) {
        this.ModifyBy = ModifyBy;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int CategoryID) {
        this.CategoryID = CategoryID;
    }
    
    
    Connection cnn; //ket noi
    Statement stm; //thuc thi cac cau lenh sql
    PreparedStatement pstm;
    ResultSet rs; //luu tru va xu ly du lieu
    private void connect() {
        try {
            cnn = (new DBContext()).connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail!");
            }
        } catch (Exception e) {
        }
    }
    
    public void AddNews(){
        try {
            cnn = (new DBContext()).connection;
            String strSelect = "insert into News (Title,CreatedDate,CreatedBy,Content,CategoryID) values (?,?,?,?,?)";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, title);
            pstm.setString(2, CreateDate);
            pstm.setString(3, CreateBy);
            pstm.setString(4, Content);
            pstm.setInt(5, CategoryID);
             pstm.execute();
        } catch (Exception e) {
            System.out.println("checkAdd:" + e.getMessage());
        }
    }
    
    public ArrayList<News> GetNews(){
        ArrayList<News> data = new ArrayList<News>();
        try {
            String strSelect = "select * from News";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                int categoryId = rs.getInt(3);
                String createDate = rs.getString(4);
                String createBy = rs.getString(6);
                String content = rs.getString("Content");
                data.add(new News(id,title, createDate, "", createBy, "", content, categoryId));
            }
        } catch (Exception e) {
            System.out.println("getNews(): " + e.getMessage());
        }
        return data;
    }

    public void GetNews(int id) {
        try {
            String strSelect = "select * from News where NewsID=?";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setInt(1, id);
            rs = pstm.executeQuery();
            while (rs.next()) {
                this.id=id;
                title = rs.getString(2);
                CategoryID= rs.getInt(3);
                CreateDate = rs.getString(4);
                CreateBy = rs.getString(6);
                Content = rs.getString("Content");
            }
        } catch (Exception e) {
            System.out.println("getNews(): " + e.getMessage());
        }
    }

    public void update() {
        try {
            String strSelect = "update News set Title=? ,Content=? where NewsID=?";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setString(1, title);
            pstm.setString(2, Content);
            pstm.setInt(3, id);
            pstm.executeQuery();
            
        } catch (Exception e) {
            System.out.println("Update(): " + e.getMessage());
        }
    }

    public void delete(int id) {
        try {
            String strSelect = "delete from News where NewsID=?";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setInt(1, id);
            pstm.executeQuery();
        } catch (Exception e) {
            System.out.println("Delete(): " + e.getMessage());
        }
    }
}
