/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Peanut
 */
public class Comment {
    private int commentID;
    private int productID;
    private int rating;
    private String content;
    private String createdDate;
    private String modifiedDate;
    private int createdBy;
    private int modifiedBy;

    public Comment() {
        connect();
    }

    public Comment(int productID) {
        this.productID = productID;
        connect();
    }

    public Comment(int commentID, int productID, int rating, String content, String createdDate, String modifiedDate, int createdBy, int modifiedBy) {
        this.commentID = commentID;
        this.productID = productID;
        this.rating = rating;
        this.content = content;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        connect();
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
    
    

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(int modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    
    
    
    Connection cnn; //ket noi
    Statement stm; //thuc thi cac cau lenh sql
    PreparedStatement pstm;
    ResultSet rs; //luu tru va xu ly du lieu

    private void connect() {
        try {
            cnn = (new DBContext()).connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail!");
            }
        } catch (Exception e) {
        }
    }

    public ArrayList<Comment> getListComment(int pID) {
        ArrayList<Comment> data = new ArrayList<Comment>();
        try {
            String strSelect = "select * from Comment where ProductID = ?";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setInt(1, pID);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int commentID = rs.getInt(1);
                int productID = rs.getInt(2);
                int rating = rs.getInt(3);
                String content = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                int createdBy = rs.getInt(7);
                int modifiedBy = rs.getInt(8);
                data.add(new Comment(commentID, productID, rating, content, createdDate, modifiedDate, createdBy, modifiedBy));
            }
        } catch (Exception e) {
            System.out.println("getListComment(): " + e.getMessage());
        }
        return data;
    }

    public void deleteComment(int commentID) {
        try {
            String strDelete = "DELETE FROM Comment WHERE CommentID = ? ";
            pstm = cnn.prepareStatement(strDelete);
            pstm.setInt(1, commentID);
            pstm.execute();
            System.out.println("Comment Deleted!!");
        } catch (Exception e) {
            System.out.println("deleteComment(): " + e.getMessage());
        }
    }

    public ArrayList<Comment> getListCommentOldToNew(int pID) {
        ArrayList<Comment> data = new ArrayList<Comment>();
        try {
            String strSelect = "select * from Comment where ProductID = ? order by CreatedDate";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setInt(1, pID);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int commentID = rs.getInt(1);
                int productID = rs.getInt(2);
                int rating = rs.getInt(3);
                String content = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                int createdBy = rs.getInt(7);
                int modifiedBy = rs.getInt(8);
                data.add(new Comment(commentID, productID, rating, content, createdDate, modifiedDate, createdBy, modifiedBy));
            }
        } catch (Exception e) {
            System.out.println("getListCommentOldToNew(): " + e.getMessage());
        }
        return data;
    }

    public ArrayList<Comment> getListCommentNewToOld(int pID) {
        ArrayList<Comment> data = new ArrayList<Comment>();
        try {
            String strSelect = "select * from Comment where ProductID = ? order by CreatedDate desc";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setInt(1, pID);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int commentID = rs.getInt(1);
                int productID = rs.getInt(2);
                int rating = rs.getInt(3);
                String content = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                int createdBy = rs.getInt(7);
                int modifiedBy = rs.getInt(8);
                data.add(new Comment(commentID, productID, rating, content, createdDate, modifiedDate, createdBy, modifiedBy));
            }
        } catch (Exception e) {
            System.out.println("getListCommentNewToOld(): " + e.getMessage());
        }
        return data;
    }
}
