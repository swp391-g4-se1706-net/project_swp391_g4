/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author CAT PHUONG
 */
public class CouponDetail {
    private int id;
    private int productDiscount, productRequired;

    public CouponDetail() {
        connect();
    }

    public CouponDetail(int id, int productDiscount, int productRequired) {
        this.id = id;
        this.productDiscount = productDiscount;
        this.productRequired = productRequired;
        connect();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(int productDiscount) {
        this.productDiscount = productDiscount;
    }

    public int getProductRequired() {
        return productRequired;
    }

    public void setProductRequired(int productRequired) {
        this.productRequired = productRequired;
    }
        
    Connection cnn; //ket noi
    Statement stm; //thuc thi cac cau lenh sql
    PreparedStatement pstm;
    ResultSet rs; //luu tru va xu ly du lieu

    private void connect() {
        try {
            cnn = (new DBContext()).connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail!");
            }
        } catch (Exception e) {
            System.out.println("connect: " + e.getMessage());
        }
    }

    public void deleteCouponDetail(int id) {
        connect();
        try {
            String strDelete = "DELETE FROM CouponDetail WHERE CouponID = ? ";
            pstm = cnn.prepareStatement(strDelete);
            pstm.setInt(1, id);
            pstm.execute();
            System.out.println("CouponDetail Deleted!!");
        } catch (Exception e) {
            System.out.println("deleteCouponDetail(): " + e.getMessage());
        }
    }

    public void addCouponDetail(int id, String[] productRequired, String[] productDiscounted) {
        connect();
        int count=0;
        for (String pd : productDiscounted) {
            for (String pr : productRequired) {
                try {
                    count++;
                    String strInsert = "INSERT [dbo].[CouponDetail] ([CouponID], [Product_discounted], "
                            + "[Product_required]) "
                            + "VALUES (?, ?, ?) ";
                    pstm = cnn.prepareStatement(strInsert);
                    pstm.setInt(1, id);
                    pstm.setInt(2, Integer.parseInt(pd));
                    pstm.setInt(3, Integer.parseInt(pr));
                    pstm.execute();
                } catch (Exception e) {
                    System.out.println("Attempt: "+ count +" pd = " + pd + " pr = " + pr);
                    System.out.println("addCouponDetail(): " + e.getMessage());
                }
            }
        }
    }

    @Override
    public String toString() {
        return "CouponDetail{" + "id=" + id + ", productDiscount=" + productDiscount + ", productRequired=" + productRequired + '}';
    }
    
    

}
