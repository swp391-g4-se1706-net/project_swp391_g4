/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author minhn
 */
public class Orders {

    int orderID, customerID, productID, quantity;
    double price, totalPrice;
    String purchaseDate, note;
    String priceConfig, totalPriceConfig;

    public Orders() {
        connect();
    }

    //Thong tin cho phan hien thi thong tin co ban cua cac don hang:
    public Orders(int orderID, int customerID, double totalPrice, String purchaseDate) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.totalPrice = totalPrice;
        this.purchaseDate = purchaseDate;
        connect();
    }

    public Orders(int orderID, int customerID, String purchaseDate, String totalPriceConfig) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.purchaseDate = purchaseDate;
        this.totalPriceConfig = totalPriceConfig;
        connect();
    }  

    //Thong tin chi tiet cua mot don hang:
    public Orders(int orderID, int customerID, int productID, int quantity, double price, String purchaseDate, String note, double totalPrice) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.productID = productID;
        this.quantity = quantity;
        this.price = price;
        this.purchaseDate = purchaseDate;
        this.note = note;
        this.totalPrice = totalPrice;
        connect();
    }

    public Orders(int orderID, int customerID, int productID, int quantity, String purchaseDate, String note, String priceConfig, String totalPriceConfig) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.productID = productID;
        this.quantity = quantity;
        this.purchaseDate = purchaseDate;
        this.note = note;
        this.priceConfig = priceConfig;
        this.totalPriceConfig = totalPriceConfig;
        connect();
    }

    
    
    

    public String getTotalPriceConfig() {
        return totalPriceConfig;
    }

    public void setTotalPriceConfig(String totalPriceConfig) {
        this.totalPriceConfig = totalPriceConfig;
    }

    public String getPriceConfig() {
        return priceConfig;
    }

    public void setPriceConfig(String priceConfig) {
        this.priceConfig = priceConfig;
    }
    
    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    Connection cnn; //ket noi
    Statement stm; //thuc thi cac cau lenh sql
    PreparedStatement pstm;
    ResultSet rs; //luu tru va xu ly du lieu

    private void connect() {
        try {
            cnn = (new DBContext()).connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail!");
            }
        } catch (Exception e) {
        }
    }

    //Used for displaying the list of all of the orders in the history
    public ArrayList<Orders> getOrderList() {
        ArrayList<Orders> data = new ArrayList<Orders>();
        try {
            String strSelect = "select OrderID, CustomerID, PurchaseDate, TotalPrice from [Order] order by PurchaseDate desc";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int customerID = rs.getInt(2);
                String purchaseDate = "";
                if (rs.getDate(3) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(3));
                }
                double totalPrice = rs.getDouble(4);
                String tprice = moneyconfig((int)totalPrice);
                data.add(new Orders(orderID, customerID, purchaseDate, tprice));
            }
        } catch (Exception e) {
            System.out.println("getOrderList: " + e.getMessage());
        }
        return data;
    }

    //Used for displaying the list of recent orders within the General tab
    public ArrayList<Orders> getOrderListLimitted() {
        ArrayList<Orders> data = new ArrayList<Orders>();
        try {
            int count = 0;
            String strSelect = "select OrderID, CustomerID, PurchaseDate, TotalPrice from [Order] order by PurchaseDate desc";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next() & count <= 6) {
                int orderID = rs.getInt(1);
                int customerID = rs.getInt(2);
                String purchaseDate = "";
                if (rs.getDate(3) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(3));
                }
                double totalPrice = rs.getDouble(4);
                String tprice = moneyconfig((int)totalPrice);
                data.add(new Orders(orderID, customerID, purchaseDate, tprice));
                count++;
            }
        } catch (Exception e) {
            System.out.println("getOrderList: " + e.getMessage());
        }
        return data;
    }

    //Used for getting the details of a specific order
    public ArrayList<Orders> getOrderDetails(int id) {
        ArrayList<Orders> data = new ArrayList<Orders>();
        try {
            String strSelect = "select [Order].OrderID, CustomerID, ProductID, "
                    + "Quantity, Price, PurchaseDate, Note, TotalPrice from OrderDetail "
                    + "join [Order] on OrderDetail.OrderID = [Order].OrderID where [Order].OrderID = '" + id + "'";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int customerID = rs.getInt(2);
                int productID = rs.getInt(3);
                int quantity = rs.getInt(4);
                double price = rs.getDouble(5);
                String purchaseDate = "";
                if (rs.getDate(6) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(6));
                }
                String note = rs.getString(7);
                double totalPrice = rs.getDouble(8);
                String cprice = moneyconfig((int)price);
                String tprice = moneyconfig((int)totalPrice);
                data.add(new Orders(orderID, customerID, productID, quantity, purchaseDate, note, cprice, tprice));
            }
        } catch (Exception e) {
            System.out.println("getOrderDetails: " + e.getMessage());
        }
        return data;
    }

    public int todaySale() {
        int sale = 0;
        try {
            String strSelect = "select TotalPrice, PurchaseDate from [Order]";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                double price = rs.getDouble(1);
                String purchaseDate = "";
                if (rs.getDate(2) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(2));
                }
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                if (date.equals(LocalDate.now())) {
                    sale += price;
                }
            }
        } catch (Exception e) {
            System.out.println("getTodaySale: " + e.getMessage());
        }
        return sale;
    }

    public int monthlySale() {
        int sale = 0;
        LocalDate currentDate = LocalDate.now();
        int currentMonth = currentDate.getMonthValue();
        try {
            String strSelect = "select TotalPrice, PurchaseDate from [Order]";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                double totalPrice = rs.getDouble(1);
                String purchaseDate = "";
                if (rs.getDate(2) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(2));
                }
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                int month = date.getMonthValue();
                if (month == currentMonth) {
                    sale += totalPrice;
                }
            }
        } catch (Exception e) {
            System.out.println("getTodaySale: " + e.getMessage());
        }
        return sale;
    }

    public int lastYearSale() {
        int sale = 0;
        LocalDate currentDate = LocalDate.now();
        int lastYear = currentDate.getYear() - 1;
        try {
            String strSelect = "select TotalPrice, PurchaseDate from [Order]";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                double price = rs.getDouble(1);
                String purchaseDate = "";
                if (rs.getDate(2) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(2));
                }
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                int year = date.getYear();
                if (year == lastYear) {
                    sale += price;
                }
            }
        } catch (Exception e) {
            System.out.println("getTodaySale: " + e.getMessage());
        }
        return sale;
    }

    public int yearSale() {
        int sale = 0;
        LocalDate currentDate = LocalDate.now();
        int currentYear = currentDate.getYear();
        try {
            String strSelect = "select TotalPrice, PurchaseDate from [Order]";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                double price = rs.getDouble(1);
                String purchaseDate = "";
                if (rs.getDate(2) != null) {
                    java.sql.Date sqlDate = rs.getDate(2);
                    java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
                    purchaseDate = new SimpleDateFormat("dd-MM-yyyy").format(utilDate);
                }
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                int year = date.getYear();
                if (year == currentYear) {
                    sale += price;
                }
            }
        } catch (Exception e) {
            System.out.println("getTodaySale: " + e.getMessage());
        }
        System.out.println(sale);
        return sale;
    }

    public ArrayList<Orders> getTodayOrderList() {
        ArrayList<Orders> data = new ArrayList<Orders>();
        try {
            String strSelect = "select OrderID, CustomerID, PurchaseDate, TotalPrice from [Order] order by PurchaseDate desc";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int customerID = rs.getInt(2);
                String purchaseDate = "";
                if (rs.getDate(3) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(3));
                }
                double totalPrice = rs.getDouble(4);
                String tprice = moneyconfig((int)totalPrice);
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                if (date.equals(LocalDate.now())) {
                    data.add(new Orders(orderID, customerID, purchaseDate, tprice));
                }
            }
        } catch (Exception e) {
            System.out.println("getTodayOrder: " + e.getMessage());
        }
        return data;
    }

    public ArrayList<Orders> getMonthOrderList() {
        ArrayList<Orders> data = new ArrayList<Orders>();
        LocalDate currentDate = LocalDate.now();
        int currentMonth = currentDate.getMonthValue();
        try {
            String strSelect = "select OrderID, CustomerID, PurchaseDate, TotalPrice from [Order] order by PurchaseDate desc";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int customerID = rs.getInt(2);
                String purchaseDate = "";
                if (rs.getDate(3) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(3));
                }
                double totalPrice = rs.getDouble(4);
                String tprice = moneyconfig((int)totalPrice);
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                int month = date.getMonthValue();
                if (month == currentMonth) {
                    data.add(new Orders(orderID, customerID, purchaseDate, tprice));
                }
            }
        } catch (Exception e) {
            System.out.println("getMonthOrder: " + e.getMessage());
        }
        return data;
    }

    public ArrayList<Orders> getLastYearOrderList() {
        ArrayList<Orders> data = new ArrayList<Orders>();
        LocalDate currentDate = LocalDate.now();
        int lastYear = currentDate.getYear() - 1;
        try {
            String strSelect = "select OrderID, CustomerID, PurchaseDate, TotalPrice from [Order] order by PurchaseDate desc";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int customerID = rs.getInt(2);
                String purchaseDate = "";
                if (rs.getDate(3) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(3));
                }
                double totalPrice = rs.getDouble(4);
                String tprice = moneyconfig((int)totalPrice);
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                int year = date.getYear();
                if (year == lastYear) {
                    data.add(new Orders(orderID, customerID, purchaseDate, tprice));
                }
            }
        } catch (Exception e) {
            System.out.println("getMonthOrder: " + e.getMessage());
        }
        return data;
    }

    public ArrayList<Orders> getYearOrderList() {
        ArrayList<Orders> data = new ArrayList<Orders>();
        LocalDate currentDate = LocalDate.now();
        int currentYear = currentDate.getYear();
        try {
            String strSelect = "select OrderID, CustomerID, PurchaseDate, TotalPrice from [Order] order by PurchaseDate desc";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int orderID = rs.getInt(1);
                int customerID = rs.getInt(2);
                String purchaseDate = "";
                if (rs.getDate(3) != null) {
                    SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
                    purchaseDate = f.format(rs.getDate(3));
                }
                double totalPrice = rs.getDouble(4);
                String tprice = moneyconfig((int)totalPrice);
                LocalDate date = LocalDate.parse(purchaseDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                int year = date.getYear();
                if (year == currentYear) {
                    data.add(new Orders(orderID, customerID, purchaseDate, tprice));
                }
            }
        } catch (Exception e) {
            System.out.println("getMonthOrder: " + e.getMessage());
        }
        return data;
    }  
    
    public String moneyconfig(int today) {
        String old = "" + today;
        String temp ="";
        int count = 0;
        for (int i = old.length()-1; i >= 0; i--) {
            if(count%3 == 0 && i!=old.length()-1){
                temp += "," + old.charAt(i);
            }else{
                temp += old.charAt(i);
            }count++;
        }
        return reverseString(temp);
    }
    
    public String reverseString(String temp){
        String a = "";
        for (int i = temp.length()-1; i >= 0; i--) {
            a += temp.charAt(i);
        }
        return a;
    }

}
