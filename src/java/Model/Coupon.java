/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author CAT PHUONG
 */
public class Coupon {

    private String name;
    private int id;
    private String message;
    private Date valid_from, valid_to;
    private String createdDate;
    private String modifiedDate;
    private String createdBy;
    private String modifiedBy;
    private boolean free_check, type;
    private ArrayList<CouponDetail> couponDetail;
    private int bill_condition, max_value;
    private String value;

    public Coupon() {
        connect();
    }

    public Coupon(String name, int id, String message, Date valid_from, Date valid_to, String createdDate, String modifiedDate, String createdBy, String modifiedBy, boolean free_check, ArrayList<CouponDetail> couponDetail, int bill_condition, int max_value, String value, boolean type) {
        this.name = name;
        this.id = id;
        this.message = message;
        this.valid_from = valid_from;
        this.valid_to = valid_to;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.free_check = free_check;
        this.couponDetail = couponDetail;
        this.bill_condition = bill_condition;
        this.max_value = max_value;
        this.value = value;
        this.type = type;
        connect();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getValid_from() {
        return valid_from;
    }

    public void setValid_from(Date valid_from) {
        this.valid_from = valid_from;
    }

    public Date getValid_to() {
        return valid_to;
    }

    public void setValid_to(Date valid_to) {
        this.valid_to = valid_to;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isFree_check() {
        return free_check;
    }

    public void setFree_check(boolean free_check) {
        this.free_check = free_check;
    }

    public ArrayList<CouponDetail> getCouponDetail() {
        return couponDetail;
    }

    public void setCouponDetail(ArrayList<CouponDetail> couponDetail) {
        this.couponDetail = couponDetail;
    }

    public int getBill_condition() {
        return bill_condition;
    }

    public void setBill_condition(int bill_condition) {
        this.bill_condition = bill_condition;
    }

    public int getMax_value() {
        return max_value;
    }

    public void setMax_value(int max_value) {
        this.max_value = max_value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    Connection cnn; //ket noi
    Statement stm; //thuc thi cac cau lenh sql
    PreparedStatement pstm;
    ResultSet rs; //luu tru va xu ly du lieu

    private void connect() {
        try {
            cnn = (new DBContext()).connection;
            if (cnn != null) {
                System.out.println("Connect success");
            } else {
                System.out.println("Connect fail!");
            }
        } catch (Exception e) {
        }
    }

    public ArrayList<Coupon> getAllCoupon() {
        connect();
        ArrayList<Coupon> data = new ArrayList<>();
        try {
            String strSelect = "select * from Coupon ";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String message = rs.getString(3);
                Date valid_from = rs.getDate(4);
                Date valid_to = rs.getDate(5);
                boolean free_check = rs.getBoolean(6);
                String createdDate = rs.getString(7);
                String modifiedDate = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String value = rs.getString(11);
                int bill_condition = rs.getInt(12);
                int max_value = rs.getInt(13);
                ArrayList<CouponDetail> couponDetail = new ArrayList<>();
                boolean type = false;

                Coupon c = new Coupon(name, id, message, valid_from, valid_to, createdDate, modifiedDate, createdBy, modifiedBy, !free_check, couponDetail, bill_condition, max_value, value, type);
                data.add(c);
            }
        } catch (Exception e) {
            System.out.println("getAllCoupon(): " + e.getMessage());
        }

        for (Coupon coupon : data) {
            coupon.couponDetail = getCouponDetailByID(coupon.id);
            for (CouponDetail cd : coupon.couponDetail) {
                if (cd.getProductDiscount() != 0) {
                    coupon.type = true;
                }
            }
        }
        return data;
    }

    public int addCoupon(String name, String message, LocalDate valid_from, LocalDate valid_to, String value, int bill_condition, int max_value) {
        connect();
        try {
            String strInsert = "INSERT [dbo].[Coupon] ([Name], [Message], [valid_from], [valid_to], [free_check],"
                    + " [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [value], [bill_condition], [max_value])  "
                    + "VALUES (? , ?, ?, ?, 0, ?, ?, 2, 2, ?, ?, ?)";
            pstm = cnn.prepareStatement(strInsert);
            pstm.setString(1, name);
            pstm.setString(2, message);
            pstm.setString(3, valid_from.toString());
            pstm.setString(4, valid_from.toString());
            pstm.setString(5, LocalDate.now().toString());
            pstm.setString(6, LocalDate.now().toString());
            pstm.setString(7, value);
            pstm.setInt(8, bill_condition);
            pstm.setInt(9, max_value);
            pstm.execute();
        } catch (Exception e) {
            System.out.println("addCoupon(): " + e.getMessage());
        }

        return getLastID();
    }

    public void updateCoupon(int id, String name, String message, LocalDate valid_from, LocalDate valid_to, String value, String bill_condition, String max_value, String[] productRequired, String[] productDiscounted) {
        connect();
        CouponDetail cd = new CouponDetail();
        cd.deleteCouponDetail(id);
        cd.addCouponDetail(id, productRequired, productDiscounted);
        try {
            String strUpdate = "UPDATE Coupon\n"
                    + "SET Name = ?, Message = ?, valid_from = ?,valid_to = ?,[ModifiedDate] = ? \n"
                    + "WHERE CouponID =? ";
            pstm = cnn.prepareStatement(strUpdate);
            pstm.setString(1, name);
            pstm.setString(2, message);
            pstm.setDate(3, Date.valueOf(valid_from));
            pstm.setDate(4, Date.valueOf(valid_to));
            pstm.setString(5, LocalDate.now().toString());
            pstm.setInt(6, id);
            pstm.execute();
            System.out.println("Coupon Update!!");
        } catch (Exception e) {
            System.out.println("updateCoupon(): " + e.getMessage());
        }
    }

    public ArrayList<CouponDetail> getCouponDetailByID(int id) {
        connect();
        ArrayList<CouponDetail> dataDetail = new ArrayList<>();
        try {
            String strSelect = "select * from CouponDetail where CouponID = ? ";
            pstm = cnn.prepareStatement(strSelect);
            pstm.setInt(1, id);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int productDiscounted = rs.getInt(2);
                int productRequired = rs.getInt(3);
                dataDetail.add(new CouponDetail(id, productDiscounted, productRequired));
            }
        } catch (Exception e) {
            System.out.println("getCouponDetailByID(): " + e.getMessage());
        }
        return dataDetail;
    }

    @Override
    public String toString() {
        return "Coupon{" + "name=" + name + ", id=" + id + ", message=" + message + ", valid_from=" + valid_from + ", valid_to=" + valid_to + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", createdBy=" + createdBy + ", modifiedBy=" + modifiedBy + ", free_check=" + free_check + ", type=" + type + ", couponDetail=" + couponDetail + ", bill_condition=" + bill_condition + ", max_value=" + max_value + ", value=" + value + '}';
    }

    private int getLastID() {
        connect();
        try {
            String strSelect = "select CouponID from Coupon order by CouponID desc";
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                System.out.println(id);
                return id;
            }
        } catch (Exception e) {
            System.out.println("getLastID(): " + e.getMessage());
        }
        return -1;
    }
    
    

}
