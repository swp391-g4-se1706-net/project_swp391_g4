/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Model.Customer;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Peanut
 */
public class RegisterController extends HttpServlet {

    public static final String registerError = "Customer already exist!! Please sign up another username!";
    public static final String passwordError = "The password you confirmed is not equal to the password! Reenter password to sign up!";
    public static final String registerSuccess = "Account register successfully!";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("submitRegister") != null) {
            if (req.getParameter("name").isEmpty() || req.getParameter("email").isEmpty() || req.getParameter("password").isEmpty()
                    || req.getParameter("re_password").isEmpty() || req.getParameter("dob").isEmpty() || req.getParameter("phone").isEmpty() || req.getParameter("address").isEmpty()) {
                req.setAttribute("passwordError", passwordError);
                req.getRequestDispatcher("Register.jsp").forward(req, resp);
            }
            String name = req.getParameter("name");
            String email = req.getParameter("email");
            String password = req.getParameter("password");
            String re_password = req.getParameter("re_password");
            String dob = req.getParameter("dob");
            String phone = req.getParameter("phone");
            String address = req.getParameter("address");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();

            if (!password.equals(re_password)) {
                req.setAttribute("passwordError", passwordError);
                req.getRequestDispatcher("Register.jsp").forward(req, resp);
            } else {
                Customer cus = new Customer();
                boolean checkCustomerExist = cus.checkCustomerExist();
                if (checkCustomerExist) {
                    req.setAttribute("registerError", registerError);
                    req.getRequestDispatcher("Register.jsp").forward(req, resp);
                } else {
                    System.out.println(dob);
                    cus.register(name, email, password, dob, phone, address, dtf.format(now));
                    req.setAttribute("registerSuccess", registerSuccess);
                    req.getRequestDispatcher("login.jsp").forward(req, resp);
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

}
