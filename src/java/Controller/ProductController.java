/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Model.Category;
import Model.Product;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Peanut
 */
public class ProductController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Product> data2;
        if (req.getParameter("category").equals("all")) {
            Product pAll = new Product();
            data2 = pAll.getListProductAtStart();
            req.setAttribute("data2", data2);
            Category c = new Category();
            ArrayList<Category> data = c.getListCategory();
            req.setAttribute("data", data);
            req.getRequestDispatcher("productInfo.jsp").forward(req, resp);
        } else {
            int cid = Integer.parseInt(req.getParameter("category"));
            Product p = new Product(cid);
            data2 = p.getListProductWithCategory();
            req.setAttribute("data2", data2);
            Category g = new Category();
            ArrayList<Category> data = g.getListCategory();
            req.setAttribute("data", data);
            req.getRequestDispatcher("productInfo.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Category c = new Category();
        Product p = new Product();
        
        int totalQuantityWithCategory1 = p.getTotalQuantityWithCategory1(1);
        int totalQuantityWithCategory2 = p.getTotalQuantityWithCategory2(2);
        int totalQuantityWithCategory3 = p.getTotalQuantityWithCategory3(3);
        req.setAttribute("totalQuantity1", totalQuantityWithCategory1);
        req.setAttribute("totalQuantity2", totalQuantityWithCategory2);
        req.setAttribute("totalQuantity3", totalQuantityWithCategory3);
        
        ArrayList<Category> data = c.getListCategory();
        ArrayList<Product> data2 = p.getListProductAtStart();
        req.setAttribute("data", data);
        req.setAttribute("data2", data2);
        req.getRequestDispatcher("shop.jsp").forward(req, resp);

    }

}
